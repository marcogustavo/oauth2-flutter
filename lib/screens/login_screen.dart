import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_oauth2_mysql/screens/home_screen.dart';
import 'package:flutter_oauth2_mysql/utils/http_service.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  HttpService httpService = HttpService();
  bool _isLogging = false;
  final _formKey = GlobalKey<FormState>();

  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  Future<void> _submitLogin(context) async {
    if (_formKey.currentState.validate()) {
      setState(() {
        _isLogging = true;
      });
      try {
        await httpService.setClient(
          _emailController.text,
          _passwordController.text,
        );

        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => HomeScreen()),
        );
      } catch (error) {
        if (error is SocketException && error.message == 'Connection failed') {
          Scaffold.of(context).showSnackBar(buildSnackbar(
              'Verifique sua conexão com a internet',
              Colors.red[700],
              Colors.white,
              Duration(seconds: 4)));
        } else if (error.toString().contains('Bad credentials.')) {
          Scaffold.of(context).showSnackBar(buildSnackbar(
              'Credenciais inválidas',
              Colors.red[700]
              Colors.white,
              Duration(seconds: 4)));
        } else {
          Scaffold.of(context).showSnackBar(buildSnackbar(error.toString(),
              Colors.red[700], Colors.white, Duration(seconds: 4)));
        }
      } finally {
        setState(() {
          _isLogging = false;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: Builder(
        builder: (context) => SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  TextFormField(
                    controller: _emailController,
                    decoration: InputDecoration(
                      labelText: 'E-mail',
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter some text';
                      }
                      return null;
                    },
                  ),
                  TextFormField(
                    controller: _passwordController,
                    decoration: InputDecoration(
                      labelText: 'Senha',
                    ),
                    obscureText: true,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter some text';
                      }
                      return null;
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: RaisedButton(
                      onPressed: () => _submitLogin(context),
                      child: Text('Entrar'),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildSnackbar(
      String text, Color bgColor, Color textColor, Duration duration) {
    return SnackBar(
      content: Text(
        text,
        style: TextStyle(color: textColor, fontSize: 15.0),
        textAlign: TextAlign.center,
      ),
      backgroundColor: bgColor,
      duration: duration,
    );
  }
}
