import 'package:flutter/material.dart';
import 'package:flutter_oauth2_mysql/models/customer.dart';

class CustomersScreen extends StatefulWidget {
  @override
  _CustomersScreenState createState() => _CustomersScreenState();
}

class _CustomersScreenState extends State<CustomersScreen> {
  Future<List<Customer>> futureCustomers;

  @override
  void initState() {
    super.initState();
    futureCustomers = Customer.fetchCustomers(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Customers'),
      ),
      body: Container(
        child: FutureBuilder<List<Customer>>(
          future: futureCustomers,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              List<Customer> data = snapshot.data;
              return ListView.builder(
                  itemCount: data.length,
                  itemBuilder: (context, index) {
                    Customer customer = data[index];
                    return ListTile(
                      title: Text(customer.name),
                      subtitle: Text(customer.id.toString()),
                    );
                  });
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }

            // By default, show a loading spinner.
            return CircularProgressIndicator();
          },
        ),
      ),
    );
  }
}
