import 'package:flutter/material.dart';
import 'package:flutter_oauth2_mysql/screens/customers_screen.dart';
import 'package:flutter_oauth2_mysql/screens/login_screen.dart';
import 'package:flutter_oauth2_mysql/screens/players_screen.dart';
import 'package:flutter_oauth2_mysql/utils/http_service.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Text('Bem-vindo!'),
              RaisedButton(
                child: Text('Customers (restricted)'),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => CustomersScreen()),
                  );
                },
              ),
              RaisedButton(
                child: Text('Players (not restricted)'),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => PlayersScreen()),
                  );
                },
              ),
              RaisedButton(
                child: Text('Logout'),
                onPressed: () async {
                  await HttpService().logout();
                  Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(builder: (context) => LoginScreen()),
                    (Route<dynamic> route) => false,
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
