import 'package:flutter/material.dart';
import 'package:flutter_oauth2_mysql/models/player.dart';

class PlayersScreen extends StatefulWidget {
  @override
  _PlayersScreenState createState() => _PlayersScreenState();
}

class _PlayersScreenState extends State<PlayersScreen> {
  Future<List<Player>> futurePlayers;

  @override
  void initState() {
    super.initState();
    futurePlayers = Player.fetchPlayers();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Players'),
      ),
      body: Container(
        child: FutureBuilder<List<Player>>(
          future: futurePlayers,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              List<Player> data = snapshot.data;
              return ListView.builder(
                  itemCount: data.length,
                  itemBuilder: (context, index) {
                    Player player = data[index];
                    return ListTile(
                      title: Text(player.name),
                      subtitle: Text(player.id.toString()),
                    );
                  });
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }

            // By default, show a loading spinner.
            return CircularProgressIndicator();
          },
        ),
      ),
    );
  }
}
