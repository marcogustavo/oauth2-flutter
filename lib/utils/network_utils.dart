class NetworkUtils {
  static final urlBase = 'http://10.0.2.2/';
  static final serverApi = 'api/';
  static final tokenEndpoint = 'oauth/token';

  static String clientIdentifier = 'fooClientIdPassword';
  static String clientSecret = 'secret';

  static String storageKeyMobileToken = 'token';
}
