import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_oauth2_mysql/screens/login_screen.dart';
import 'package:flutter_oauth2_mysql/utils/http_service.dart';
import 'package:oauth2/oauth2.dart';

class Customer {
  int id;
  String name;

  Customer({
    this.id,
    this.name,
  });

  factory Customer.fromJson(Map<String, dynamic> json) {
    return Customer(
      id: json['id'],
      name: json['name'],
    );
  }

  static Future<List<Customer>> fetchCustomers(BuildContext context) async {
    try {
      Client client = await HttpService().getClient();
      final response = await client.get('http://10.0.2.2/api/customers');

      if (response.statusCode == 200) {
        List jsonResponse = json.decode(utf8.decode(response.bodyBytes));
        return jsonResponse.map((job) => Customer.fromJson(job)).toList();
      } else {
        throw Exception('Failed to load customers');
      }
    } catch (error) {
      if (error is AuthorizationException) {
        await HttpService().logout();
        Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => LoginScreen()),
          (Route<dynamic> route) => false,
        );
      } else {
        throw error;
      }
    }
  }
}
