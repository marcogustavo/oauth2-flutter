import 'dart:convert';

import 'package:flutter_oauth2_mysql/utils/http_service.dart';
import 'package:oauth2/oauth2.dart';

class Player {
  int id;
  String name;

  Player({
    this.id,
    this.name,
  });

  factory Player.fromJson(Map<String, dynamic> json) {
    return Player(
      id: json['id'],
      name: json['name'],
    );
  }

  static Future<List<Player>> fetchPlayers() async {
    Client client = await HttpService().getClient();
    final response = await client.get('http://10.0.2.2/api/players');

    if (response.statusCode == 200) {
      List jsonResponse = json.decode(utf8.decode(response.bodyBytes));
      return jsonResponse.map((job) => Player.fromJson(job)).toList();
    } else {
      throw Exception('Failed to load players');
    }
  }
}
