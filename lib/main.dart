import 'package:flutter/material.dart';
import 'package:flutter_oauth2_mysql/screens/home_screen.dart';
import 'package:flutter_oauth2_mysql/screens/login_screen.dart';
import 'package:flutter_oauth2_mysql/screens/splash_screen.dart';
import 'package:flutter_oauth2_mysql/utils/http_service.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(visualDensity: VisualDensity.adaptivePlatformDensity),
      home: _buildHome(),
    );
  }

  Widget _buildHome() {
    return FutureBuilder(
      future: HttpService().ensureLoggedIn(),
      builder: (ctx, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return SplashScreen();
        }

        bool isLoggedIn = snapshot.data;
        return isLoggedIn ? HomeScreen() : LoginScreen();
      },
    );
  }
}
